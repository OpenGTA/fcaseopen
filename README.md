fcaseopen
=========

Case-insensitive `fopen` and `chdir` definition for case-sensitive file-systems.

A implementation that mixes the original repository with OpenGTA's.

Provides
--------

To be used outside function:
```c
FILE* fcaseopen(char const *path, char const *mode)
void  casechdir(char const *path)
```
